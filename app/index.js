const express = require("express");
const bodyParser = require("body-parser");

const app = express();

app.set('views', __dirname);
app.set('view engine', 'pug', { pretty: true });

app.use(bodyParser.urlencoded({ "extended": true }));
app.use(require('./database/router'));
app.use(require("./site/router"));

app.use(require("./errors/not-found"));

module.exports = app;