const express = require("express");

const router = new express.Router();

function home(req, res) {
  res.render('site/home', {
    "author": process.env.author,
    "title": process.env.title,
    "startDate": process.env.startDate,
    "github": process.env.github,
    "trello": process.env.trello
  });
}

router.get('/', home);

module.exports = router;