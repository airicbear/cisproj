const config = require("./config");
const app = require(".");
const port = 3000;

config.setup();

app.listen(port, () => {
  console.log(`App is being hosted at http://localhost:${port}.`);
});