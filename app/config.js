const config = module.exports;

const info = require("../info");
const secret = require("../secret");

function setVariable(key, options) {
  let variable = key;
  if (options.prefix) {
    variable = options.prefix + variable;
  }
  return options.prefix
    ? (options.uppercase ? options.prefix + key.toUpperCase() : options.prefix + key)
    : (options.uppercase ? key.toUpperCase() : key);
}

function setVariables(json, options) {
  for (key in json) {
    process.env[setVariable(key, options)] = json[key];
  }
}

config.setup = function (options = {}) {
  setVariables(info, options);
  setVariables(secret, options);
};
