const express = require("express");
const router = new express.Router();
const render = require("./router/render");
const editDatabase = require("./router/editDatabase");
const alterTable = require("./router/alterTable");
const editTable = require("./router/editTable");
const middleware = require("./router/middleware");
const directory = "database";
const tableName = "tableName";

// Database page
router.get(`/${directory}/`, middleware.createDatabase, render.renderDatabase);
router.post(`/CreateTable/`, editDatabase.createTable);
router.post(`/DropTable/:${tableName}`, editDatabase.dropTable);

// Alter table page
router.get(`/${directory}/:${tableName}/AlterTable`,
  middleware.listTableColumns, middleware.listForeignKeys, render.renderTableAlter
);
router.post(`/AddColumn/:${tableName}`, alterTable.addColumn);
router.post(`/DeleteColumn/:${tableName}`, alterTable.deleteColumn);
router.post(`/ModifyColumn/:${tableName}`, alterTable.modifyColumn);
router.post(`/AddForeignKey/:${tableName}`, alterTable.addForeignKey);
router.post(`/DropForeignKey/:${tableName}`, alterTable.dropForeignKey);
router.post(`/RenameTable/:${tableName}`, alterTable.renameTable);

// Table page
router.get(`/${directory}/:${tableName}`,
  middleware.listTableColumns, render.renderTable);
router.get(`/SaveTable/:${tableName}`, editTable.saveTable);
router.post(`/SaveTable/:${tableName}`, editTable.saveTable);
router.post(`/DeleteTable/:${tableName}`, editTable.deleteTable);
router.post(`/DeleteEntry/:${tableName}/:id`, editTable.deleteEntry);
router.post(`/AddEntry/:${tableName}`, editTable.addEntry);

// Table fullscreen page
router.get(`/${directory}/:${tableName}/fullscreen`, render.renderTableFullscreen);

// Entry page
router.get(`/${directory}/:${tableName}/:id`, render.renderEntry);
router.post(`/updateentry/:${tableName}/:id`, editTable.updateEntry);

module.exports = router;
