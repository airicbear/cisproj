const alterTable = module.exports;

const db = require("./db");
const render = require("./render");

const directory = "database";

alterTable.addColumn = function (req, res) {
  const queryString = `
  ALTER TABLE ${req.params.tableName}
          ADD ${req.body.columnName} ${req.body.columnType};`;
  const query = db.query(queryString, (error, result) => {
    if (error) {
      render.renderError(res, error);
    }
    if (result) {
      res.redirect(`/${directory}/${req.params.tableName}/AlterTable`);
    }
  });
}

alterTable.deleteColumn = function (req, res) {
  const queryString = `
  ALTER TABLE ${req.params.tableName} 
  DROP COLUMN ${req.body.columnName};`;
  const query = db.query(queryString, (error, result) => {
    if (error) {
      render.renderError(res, error);
    }
    if (result) {
      res.redirect(`/${directory}/${req.params.tableName}/AlterTable`);
    }
  });
}

alterTable.modifyColumn = function (req, res) {
  const queryString = `
    ALTER TABLE ${req.params.tableName}
         CHANGE ${req.body.columnName}
                ${req.body.newColumnName} ${req.body.newColumnType};`;
  const query = db.query(queryString, (error, result) => {
    if (error) {
      render.renderError(res, error);
    }
    if (result) {
      res.redirect(`/${directory}/${req.params.tableName}/AlterTable`);
    }
  });
}

alterTable.addForeignKey = function (req, res) {
  const queryString = `
    ALTER TABLE ${req.params.tableName}
            ADD CONSTRAINT FK_${req.body.foreignKey}
                FOREIGN KEY (${req.body.foreignKey})
                REFERENCES ${req.body.referenceTable}(${req.body.referenceId});`
  const query = db.query(queryString, (error, result) => {
    if (error) {
      render.renderError(res, error);
    }
    if (result) {
      res.redirect(`/${directory}/${req.params.tableName}/AlterTable`);
    }
  });
}

alterTable.dropForeignKey = function (req, res) {
  const queryString = `
    ALTER TABLE ${req.params.tableName}
           DROP FK_${req.body.foreignKey};`
  const query = db.query(queryString, (error, result) => {
    if (error) {
      render.renderError(res, error);
    }
    if (result) {
      res.redirect(`/${directory}/${req.params.tableName}/AlterTable`);
    }
  });
}

alterTable.renameTable = function (req, res) {
  const queryString = `
    RENAME TABLE ${req.params.tableName}
              TO ${req.body.newTableName};`
  const query = db.query(queryString, (error, result) => {
    if (error) {
      render.renderError(res, error);
    }
    if (result) {
      res.redirect(`/${directory}/${req.body.newTableName}/AlterTable`);
    }
  });
}