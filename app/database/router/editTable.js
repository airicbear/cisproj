const editTable = module.exports;

const db = require("./db");
const render = require("./render");

const directory = "database";

editTable.saveTable = function (req, res) {
  const queryString = `SELECT * FROM ${req.params.tableName};`;
  const query = db.query(queryString, (error, results) => {
    if (error) {
      render.renderError(res, error);
    }
    if (results) {
      res.send(results);
    }
  });
}

editTable.deleteTable = function (req, res) {
  const queryString = `TRUNCATE ${req.params.tableName};`;
  const query = db.query(queryString, (error, result) => {
    if (error) {
      render.renderError(res, error);
    }
    if (result) {
      res.redirect(`/${directory}/${req.params.tableName}`);
    }
  });
}

editTable.deleteEntry = function (req, res) {
  const queryString = `
  DELETE
    FROM ${req.params.tableName}
   WHERE id = ${req.params.id};`;
  const query = db.query(queryString, (error, result) => {
    if (error) {
      render.renderError(res, error);
    }
    if (result) {
      res.redirect(`/${directory}/${req.params.tableName}`);
    }
  });
}

editTable.updateEntry = function (req, res) {
  const queryString = `
  UPDATE ${req.params.tableName}
     SET ?
   WHERE id = ${req.params.id};`;
  const query = db.query(queryString, req.body, (error, result) => {
    if (error) {
      render.renderError(res, error);
    }
    if (result) {
      res.redirect(`/${directory}/${req.params.tableName}`);
    }
  });
}

editTable.addEntry = function (req, res) {
  const queryString = `
  INSERT
    INTO ${req.params.tableName}
     SET ?;`;
  const query = db.query(queryString, req.body, (error, result) => {
    if (error) {
      render.renderError(res, error);
    }
    if (result) {
      res.redirect(`/${directory}/${req.params.tableName}`);
    }
  });
}