const db = require("./db");
const render = require("./render");
const middleware = module.exports;

middleware.createDatabase = function (req, res, next) {
  const queryString = `CREATE DATABASE IF NOT EXISTS ${process.env.database}`;
  const query = db.query(queryString, (error, results) => {
    if (error) {
      render.renderError(res, error);
    }
    return next(error);
  });
}

middleware.listTableColumns = function (req, res, next) {
  const queryString = `DESCRIBE ${req.params.tableName};`;
  const query = db.query(queryString, (error, results) => {
    if (error) {
      render.renderError(res, error);
    }
    if (results) {
      req.tableColumns = [];
      for (let i = 0; i < results.length; i++) {
        req.tableColumns.push(`{
          'field': '${results[i].Field}',
          'type': '${results[i].Type}'
        }`);
      }
    }
    return next(error);
  });
};

middleware.listForeignKeys = function (req, res, next) {
  const queryString = `
  SELECT CONCAT(table_name, '.', column_name) AS 'foreign key'
       , CONCAT(referenced_table_name, '.', referenced_column_name)
         AS 'references'
    FROM information_schema.key_column_usage
   WHERE referenced_table_name
         IS NOT null;`;
  const query = db.query(queryString, (error, results) => {
    if (error) {
      render.renderError(res, error);
    }
    if (results) {
      req.foreignKeys = results;
    }
    return next(error);
  });
};
