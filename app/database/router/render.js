const render = module.exports;

const db = require("./db");
const directory = "database";

render.renderError = function (res, error) {
  res.render("errors/error", {
    "database": db.config.database,
    "error": error,
    "title": process.env.title,
  });
  console.error(`ERROR_SQL: ${error.sql}`);
};

render.renderDatabase = function (req, res) {
  const queryString = `SHOW TABLES;`;
  const query = db.query(queryString, (error, results) => {
    if (error) {
      render.renderError(res, error);
    }
    if (results) {
      res.render(`${directory}/editors/database`, {
        "directory": directory,
        "database": db.config.database,
        "results": results,
        "title": process.env.title,
      });
    }
  });
};

render.renderTableAlter = function (req, res) {
  res.render(`${directory}/editors/database/altertable`, {
    "database": db.config.database,
    "directory": directory,
    "foreignKeys": req.foreignKeys,
    "tableColumns": req.tableColumns,
    "tableName": req.params.tableName,
    "title": process.env.title,
  });
};

render.renderTable = function (req, res) {
  const queryString = `SELECT * FROM ${req.params.tableName};`;
  const query = db.query(queryString, (error, results) => {
    if (error) {
      render.renderError(res, error);
    }
    if (results) {
      const columnNames = (obj) => {
        const keys = [];
        for (const key in obj) {
          keys.push(key);
        }
        return keys;
      };
      res.render(`${directory}/editors/table`, {
        "directory": directory,
        "database": db.config.database,
        "columnNames": columnNames(results[0]),
        "tableName": req.params.tableName,
        "title": process.env.title,
        "results": results,
        "tableColumns": req.tableColumns,
      });
    }
  });
}

render.renderTableFullscreen = function (req, res) {
  const queryString = `SELECT * FROM ${req.params.tableName};`;
  const query = db.query(queryString, (error, results) => {
    if (error) {
      render.renderError(res, error);
    }
    if (results) {
      const columnNames = (obj) => {
        const keys = [];
        for (const key in obj) {
          keys.push(key);
        }
        return keys;
      };
      res.render(`${directory}/editors/table/fullscreen`, {
        "columnNames": columnNames(results[0]),
        "database": db.config.database,
        "directory": directory,
        "results": results,
        "tableName": req.params.tableName,
        "title": process.env.title,
      });
    }
  });
};

render.renderEntry = function (req, res) {
  const queryString = `
  SELECT *
    FROM ${req.params.tableName} 
   WHERE id = ${req.params.id};`;
  const query = db.query(queryString, (error, result) => {
    if (error) {
      render.renderError(res, error);
    }
    if (result) {
      let resultString = "{";
      for (field in result[0]) {
        resultString += `${field}: '${result[0][field]}', `
      }
      resultString += "}";
      resultString = [resultString];
      res.render(`${directory}/editors/entry`, {
        "database": db.config.database,
        "directory": directory,
        "id": req.params.id,
        "result": resultString,
        "tableName": req.params.tableName,
        "title": process.env.title,
      });
    }
  });
};