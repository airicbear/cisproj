function divRow(children) {
  return div({
    "className": "row"
  }, children);
}

function divCol(children) {
  return div({
    "className": "col wp-2"
  }, children);
}

function _input(name, placeholder, className) {
  return input({
    "className": className,
    "name": name,
    "placeholder": placeholder,
    "required": true,
    "type": "text",
  });
}

function _select(name, children, className) {
  return select({
    "className": className,
    "name": name,
    "required": true,
  }, children);
}

function _button(className, type, innerHTML, onclick) {
  return button({
    "className": className,
    "type": type,
    "innerHTML": innerHTML,
    "onclick": onclick,
  });
}

function _option(value, innerHTML) {
  return option({
    "value": value,
    "innerHTML": innerHTML
  });
}

const columnTypes = () => [
  _option("VARCHAR(255)", "text"),
  _option("INT", "number"),
];

function userInput(str) {
  switch (str) {
    case "PrimaryKey":
      return _input("primaryKey", "Primary key", "p-2");
    case "ColumnName":
      return _input("columnName", "Column name", "p-2 wp-1");
    case "ColumnType":
      return _select("columnType", columnTypes(), "p-2");
    case "NewColumnName":
      return _input("newColumnName", "New column name", "p-2 wp-1");
    case "NewColumnType":
      return _select("newColumnType", columnTypes(), "p-2 wp-1");
    case "ForeignKey":
      return _input("foreignKey", "Foreign key", "p-2 wp-2");
    case "ReferenceTable":
      return _input("referenceTable", "Reference table", "p-2 wp-2");
    case "ReferenceID":
      return _input("referenceId", "Reference ID", "p-2 wp-2");
    case "NewTableName":
      return _input("newTableName", "New table name", "p-2 wp-1");
    case "SubmitButton":
      return _button("p-2", "submit", "Submit");
    default:
      return _input("userInput", "User input", "p-2");
  }
}

function removeButton(id) {
  return _button("p-2 danger", "button", "Remove", () => {
    parent.removeChild(parent.children[parent.childElementCount - 1])
  });
} 

function addPrimaryKey(parent) {
  parent.appendChild(divRow([
    divCol([label({
      "innerHTML": "Primary Key",
      "for": "primaryKey"
    })]),
    divCol([userInput("PrimaryKey")])
  ], "primaryKey"));
}

function newColumn(parent) {
  const newColumn = divRow([
    divCol([userInput("ColumnName")]),
    divCol([
      userInput("ColumnType"),
      removeButton(parent.children[parent.childElementCount - 1]),
    ])
  ], "primaryKey");
  parent.appendChild(newColumn);
  newColumn.firstChild.firstChild.focus();
}

function newForeignKey(parent) {
  const newColumn = divRow([
    divCol([
      userInput("ForeignKey"),
      userInput("ReferenceTable"),
    ]),
    divCol([
      userInput("ReferenceID"),
      removeButton(parent.children[parent.childElementCount - 1]),
    ]),
  ]);
  parent.appendChild(newColumn);
  newColumn.firstChild.firstChild.focus();
}

function _alterColumnSubmit(parent, children) {
  const newColumn = divRow(children);
  parent.appendChild(newColumn);
}

function _alterFormInputs(action) {
  switch (action) {
    case "AddColumn":
      return [
        divCol([userInput("ColumnName")]),
        divCol([userInput("ColumnType")]),
      ];
    case "DeleteColumn":
      return [
        divCol([userInput("ColumnName")]),
      ];
    case "ModifyColumn":
      return [
        divCol([userInput("ColumnName")]),
        divCol([userInput("NewColumnName")]),
        divCol([userInput("NewColumnType")]),
      ];
    case "AddForeignKey":
      return [
        divCol([userInput("ForeignKey")]),
        divCol([userInput("ReferenceTable")]),
        divCol([userInput("ReferenceID")]),
      ];
    case "DropForeignKey":
      return [
        divCol([userInput("ForeignKey")]),
      ];
    case "RenameTable":
      return [
        divCol([userInput("NewTableName")]),
      ];
    default:
      return [];
  }
}

function _alterForm(action, parent) {
  const inputs = _alterFormInputs(action);
  inputs.push(userInput("SubmitButton"));
  return _alterColumnSubmit(parent, inputs);
}

function newColumnSubmit(parent, action) {
  switch (action) {
    case "AddColumn":
      return _alterForm(action, parent);
    case "ModifyColumn":
      return _alterForm(action, parent);
    case "DeleteColumn":
      return _alterForm(action, parent);
    case "AddForeignKey":
      return _alterForm(action, parent);
    case "DropForeignKey":
      return _alterForm(action, parent);
    case "RenameTable":
      return _alterForm(action, parent);
    default:
      return _alterForm(action, parent);
  }
}