const addEntry = document.getElementById("addEntry");

function inputColumnString(columnName) {
  return input({
    "type": "text",
    "className": "p-2",
    "name": columnName,
    "placeholder": columnName
  });
}

function inputColumnNumber(columnName, step = 1) {
  return input({
    "type": "number",
    "className": "p-2 w-10",
    "name": columnName,
    "placeholder": columnName,
    "step": step
  });
}

function buttonSubmit() {
  return button({
    "className": "p-2",
    "type": "submit",
    "innerHTML": "Submit"
  });
}

function addEntries() {
  for (let i = 0; i < arguments.length; i++) {
    if (arguments[i].type.includes("int")) {
      addEntry.appendChild(inputColumnNumber(arguments[i].field));
    } else if (arguments[i].type.includes("float")
      || arguments[i].type.includes("decimal")) {
      addEntry.appendChild(inputColumnNumber(arguments[i].field, 0.1));
    } else {
      addEntry.appendChild(inputColumnString(arguments[i].field));
    }
  }
  addEntry.appendChild(buttonSubmit());
}